import React from "react";
import './App.scss';

import RouterComponent from "./router/router";

function App() {
  return (
    <div className="App">
      <RouterComponent />
    </div>
  );
}

export default App;

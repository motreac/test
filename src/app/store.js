import { configureStore } from "@reduxjs/toolkit";
// import { fetchNewsMiddleware } from "../redux/middleware";
import { news } from "../redux";

export const store = configureStore({
  reducer: {
    news: news.reducer,
  },
  // middleware: [fetchNewsMiddleware],
});

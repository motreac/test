import { useCallback, useMemo } from "react";
import { useDispatch } from "react-redux";

import { Arrow } from "./icons/arrow";
import { Bookmark } from "./icons/bookmark";
import { news } from "../redux";

const days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

export const Card = ({ info, isBookmark }) => {
  const dispatch = useDispatch();

  const date = useMemo(() => {
    const fullDate = new Date(info.datetime * 1000);
    const dayName = days[fullDate.getDay()];
    return dayName + " " + fullDate.getDate();
  }, [info]);

  const addBookmark = useCallback(
    () => dispatch(news.actions.toggleBookmark(info)),
    [info, dispatch]
  );

  return (
    <div className="card">
      <img src={info.image} alt={info.headline} className="card-bg" />
      <div className="card-content">
        <div className="card-top">
          <span className="label">{info.related}</span>
        </div>
        <div className="card-bottom">
          <p className="card-title">{info.headline}</p>

          <div className="details">
            <span className="date">{date}</span>
            <span className="separator"></span>
            <span className="time">6 min read</span>
            <div className="details-actions">
              <Arrow />
              <Bookmark filled={isBookmark} onClick={addBookmark} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

import { NavLink } from "react-router-dom";
export const Header = () => {
  return (
    <nav>
      <ul>
        <li>
          <NavLink to="/">News</NavLink>
        </li>
        <li>
          <NavLink to="/bookmarks">Bookmarks</NavLink>
        </li>
      </ul>
    </nav>
  );
};

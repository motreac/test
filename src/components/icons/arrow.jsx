import * as React from "react";

export const Arrow = ({ onClick }) => (
  <svg
    width="11"
    height="14"
    viewBox="0 0 11 14"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    onClick={onClick}
  >
    <path
      d="M1.29168 12.0403V7.08434L8.83965 2.16039"
      stroke="white"
      strokeWidth="2"
      strokeLinecap="round"
      opacity="0.5"
    />
    <path
      d="M6.10881 1.55527L9.1416 1.55631L9.1405 4.93176"
      stroke="white"
      strokeWidth="2.0004"
      strokeLinecap="round"
      strokeLinejoin="round"
      opacity="0.5"
    />
  </svg>
);

import * as React from "react";

export const Bookmark = ({ filled, onClick }) => {
  if (filled) {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        id="body_1"
        width="13"
        height="14"
        onClick={onClick}
      >
        <g transform="matrix(0.3170732 0 0 0.3181818 0 0)">
          <g transform="matrix(1 0 0 1 0 0)"></g>
          <path
            transform="matrix(1 0 0 1 0 0)"
            d="M0 0L41 0L41 43.9286L20.3913 35.3824L0 43.9286L0 0z"
            stroke="none"
            fill="#FFFFFF"
            fillRule="nonzero"
            fillOpacity="0.4941176"
          />
        </g>
      </svg>
    );
  }

  return (
    <svg
      width="13"
      height="14"
      viewBox="0 0 13 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      onClick={onClick}
    >
      <path
        d="M2.76366 2.86537V10.1868L5.71326 8.95062L6.48225 8.62833L7.25245 8.94772L10.27 10.1991V2.86537H2.76366ZM0.763657 0.86537H12.27V13.1936L6.48633 10.7952L0.763657 13.1936V0.86537Z"
        fill="white"
        opacity="0.5"
      />
    </svg>
  );
};

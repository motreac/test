import React from "react";
import { useSelector } from "react-redux";
import { news } from "../../redux";
import { Card } from "../../components";

export function BookmarksPage() {
  const bookmarks = useSelector(news.selectors.bookmarks);
  if (!bookmarks.length) return null;
  return (
    <div className="cards-container">
      {bookmarks.map((bookmark) => (
        <Card key={bookmark.id} isBookmark info={bookmark} />
      ))}
    </div>
  );
}

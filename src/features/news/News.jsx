import React, { useEffect, useState, useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";

import { news } from "../../redux";
import { Card } from "../../components";

function paginate(array, page_size, page_number) {
  return array.slice((page_number - 1) * page_size, page_number * page_size);
}

const numberOfNewsOnPage = 6;
export function NewsPage() {
  const list = useSelector(news.selectors.list);
  const bookmarks = useSelector(news.selectors.bookmarks);
  const dispatch = useDispatch();

  const [searchQuery, setSearchQuery] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  useEffect(() => {
    dispatch(news.actions.fetchNews());
  }, [dispatch]);

  const searchInputchange = (e) => {
    setCurrentPage(1);
    setSearchQuery(e.target.value);
  };
  const filteredNews = useMemo(() => {
    if (!searchQuery.length) {
      return paginate(list, numberOfNewsOnPage, currentPage);
    }
    const filtered = list.filter((el) =>
      el.headline.toLowerCase().includes(searchQuery.toLocaleLowerCase())
    );
    return paginate(filtered, numberOfNewsOnPage, currentPage);
  }, [list, currentPage, searchQuery]);

  const nextPage = () => {
    if (news.length / numberOfNewsOnPage === currentPage) return;
    setCurrentPage((s) => s + 1);
  };

  const prevPage = () => {
    if (currentPage === 1) return;
    setCurrentPage((s) => s - 1);
  };
  return (
    <div>
      <input placeholder="Search" onChange={searchInputchange} />
      <div className="news-wrapper">
        <aside></aside>
        <div className="cards-container">
          {filteredNews.map((el) => (
            <Card
              key={el.id}
              info={el}
              isBookmark={bookmarks.some(({ id }) => id === el.id)}
            />
          ))}
        </div>
      </div>
      <div className="pagination-buttons">
        <button cursor="pointer" type="button" onClick={prevPage}>
          Previous Page
        </button>
        {currentPage}
        <button cursor="pointer" type="button" onClick={nextPage}>
          Next Page
        </button>
      </div>
    </div>
  );
}

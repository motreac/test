import axios from "axios";
import {
  createAction,
  createAsyncThunk,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";

const actions = {
  fetchNews: createAsyncThunk("fetchNews", async () => {
    const { data } = await axios(
      "https://finnhub.io/api/v1/company-news?symbol=AAPL&from=2021-03-01&to=2021-03-15&token=bpjsf67rh5r9328ecgvg"
    );
    return data;
  }),
  toggleBookmark: createAction("toggleBookmark"),
};

const initialState = {
  list: [],
  bookmarks: [],
};

const slice = createSlice({
  name: "news",
  initialState,
  reducers: {},
  extraReducers: {
    [actions.fetchNews.fulfilled]: (state, { payload }) => {
      state.list = payload;
    },
    [actions.fetchNews.rejected]: (state) => {
      state.list = [];
    },
    [actions.toggleBookmark]: (state, { payload }) => {
      const idx = state.bookmarks.find(({ id }) => id === payload.id);
      if (idx) {
        state.bookmarks.splice(idx, 1);
      } else {
        state.bookmarks.push(payload);
      }
    },
  },
});

const selectors = {
  list: createSelector(
    (state) => state.news.list,
    (allNews) => allNews
  ),
  bookmarks: createSelector(
    (state) => state.news.bookmarks,
    (bookmarks) => bookmarks
  ),
};

export const news = {
  selectors,
  reducer: slice.reducer,
  actions: {
    ...slice.actions,
    ...actions,
  },
};

import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import { BookmarksPage, NewsPage } from "../features";
import { Header } from "../components";

export default function RouterComponent() {
  return (
    <Router>
      <div>
        <Header />
        <Switch>
          <Route path="/bookmarks">
            <BookmarksPage />
          </Route>
          <Route exact path="/">
            <NewsPage />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
